/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.ui.activity;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import java.util.Random;

import timetable.com.gmail.pietsch.josia.timetable.R;
import timetable.com.gmail.pietsch.josia.timetable.data.TimetableDataManager;
import timetable.com.gmail.pietsch.josia.timetable.data.TimetableDataUpdateListener;
import timetable.com.gmail.pietsch.josia.timetable.ui.DialogHelper;
import timetable.com.gmail.pietsch.josia.timetable.ui.SectionsPageAdapter;
import timetable.com.gmail.pietsch.josia.timetable.ui.fragment.MenuPlanFragment;
import timetable.com.gmail.pietsch.josia.timetable.ui.fragment.TimetableFragment;


public class TimetableActivity extends AppCompatActivity {
    public static final String tag = "TimetableActivity";
    private TimetableFragment today;
    private TimetableFragment tomorrow;
    private DrawerLayout mDrawerLayout;
    DialogFragment loading;
    private ViewPager mViewPager;
    private boolean pref_secondary = false;

    private TimetableDataUpdateListener listener = new TimetableDataUpdateListener() {
        @Override
        public void onUpdate() {
            onDataUpdate();
        }
    };
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(timetable.com.gmail.pietsch.josia.timetable.R.drawable.ic_navigation);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });
        initViews();



    }
    private void initViews(){
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(mViewPager);

        try {
            pref_secondary = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("pref_secondary", false);
        } catch (Exception e) {

        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.btn_middle) {
                            today.setSecondary(false);
                            tomorrow.setSecondary(false);
                        } else if (id == R.id.btn_secundary) {
                            today.setSecondary(true);
                            tomorrow.setSecondary(true);
                        } else if (id == R.id.btn_exam) {
                            Intent i = new Intent();
                            i.setClass(getApplicationContext(), ExamPlanActivity.class);
                            startActivity(i);
                        } else if (id == R.id.btn_misc) {
                            Intent i = new Intent();
                            i.setClass(getApplicationContext(), MiscInfoActivity.class);
                            startActivity(i);
                        } else if (id == R.id.btn_info) {
                            Intent i = new Intent();
                            i.setClass(getApplicationContext(), AppInfoActivity.class);
                            startActivity(i);
                        } else if (id == R.id.btn_settings) {
                            Intent i = new Intent();
                            i.setClass(getApplicationContext(), TimetablePreferenceActivity.class);
                            startActivity(i);
                        }
                        return true;
                    }
                });
        if(pref_secondary){
            navigationView.setCheckedItem(R.id.btn_secundary);
        }else {
            navigationView.setCheckedItem(R.id.btn_middle);
        }

        if(TimetableDataManager.getInstance().getState() > TimetableDataManager.STATE_BUSY){
            today.updateText();
            tomorrow.updateText();
            today.setImage(false, pref_secondary);
            tomorrow.setImage(true, pref_secondary);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        TimetableDataManager.getInstance().addListener(listener);
        if(TimetableDataManager.getInstance().getState() == TimetableDataManager.STATE_NO_DATA){
            loadData();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimetableDataManager.getInstance().removeListener(listener);
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        today = new TimetableFragment();
        today.displayTomorrow(false);
        tomorrow = new TimetableFragment();
        tomorrow.displayTomorrow(true);

        adapter.addFragment(today, "Heute");

        adapter.addFragment(tomorrow, "Nächster Schultag");
        adapter.addFragment(new MenuPlanFragment(), "Kochplan");

        today.updateText();
        tomorrow.updateText();
        viewPager.setAdapter(adapter);
    }

    private void loadData() {
        loading = DialogHelper.createSimpleDialog("Laden...");
        loading.setCancelable(false);
        loading.show(this.getFragmentManager(), "msg1");
        TimetableDataManager.getInstance().load();
    }

    private void onDataUpdate(){
        int i = TimetableDataManager.getInstance().getState();
        if(i > TimetableDataManager.STATE_BUSY){
            if(loading != null) {
                loading.dismiss();
            }
            today.setSecondary(pref_secondary);
            tomorrow.setSecondary(pref_secondary);
        }
        today.updateText();
        tomorrow.updateText();

    }

}
