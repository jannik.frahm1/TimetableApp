/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.ui.view;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

import java.util.prefs.Preferences;

import timetable.com.gmail.pietsch.josia.timetable.R;
import timetable.com.gmail.pietsch.josia.timetable.ui.fragment.TimetableFragment;

/**
 * Created by jrpie on 15.01.2018.
 */

public class ZoomHelper implements View.OnTouchListener{

    private static final int INVALID = -1;
    private int pointerID = INVALID;
    private int pointerID2 = INVALID;
    private MotionEvent.PointerCoords p1 = null;
    private MotionEvent.PointerCoords p2 = null;
    private int offsetX = 0;
    private int offsetY = 0;
    private int centerX = 0;
    private int centerY = 0;
    private double distanceOld = 0.0f;
    private double zoomFactor = 1.0f;
    private double zoomFactorOld = 1.0f;
    private static boolean zoom = true;
    private Bitmap bitmap;
    private Bitmap tmp;
    private View v;
    private int width;
    private int height;
    public ZoomHelper(Bitmap bitmap, View v, int width, int height){
        this.bitmap = bitmap;
        this.v = v;
        updateSize(width, height);
        if(v != null){
            if(!PreferenceManager.getDefaultSharedPreferences(v.getContext()).getBoolean("pref_zoom", false)){
                zoom = false;
            }
        }
        createTmpBitmap();
    }

    public void resetZoom(){
        offsetX = 0;
        offsetY = 0;

        centerX = 0;
        centerY = 0;
        zoomFactor = 1.0f;
        zoomFactorOld = 1.0f;
        invalidateView();
    }

    public Bitmap getScaledBitmap(){
        return tmp == null ? bitmap : tmp;
    }
    private void createTmpBitmap(){
        if(tmp != null){
            tmp.recycle();
        }
        tmp = Bitmap.createScaledBitmap(bitmap, (int)(zoomFactor * width), (int)(zoomFactor * height), false);
    }
    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if(!zoom){
            return false;
        }
        int action = MotionEventCompat.getActionMasked(motionEvent);
        switch(action){
            case MotionEvent.ACTION_MOVE:
                if(pointerID == INVALID || pointerID2 == INVALID) {
                    //don't break!
                }else{
                    for(int i = 0; i < motionEvent.getPointerCount(); i++) {
                        if(motionEvent.getPointerId(i) == pointerID) {
                            if(p1 == null){
                                p1 = new MotionEvent.PointerCoords();
                            }
                            motionEvent.getPointerCoords(i, p1);
                        }else if(motionEvent.getPointerId(i) == pointerID2){
                            if(p2 == null){
                                p2 = new MotionEvent.PointerCoords();
                            }
                            motionEvent.getPointerCoords(i, p2);
                        }
                    }
                    int x1 = (int)p1.x;
                    int y1 = (int)p1.y;
                    int x2 = (int)p2.x;
                    int y2 = (int)p2.y;
                    int dx = x1-x2;
                    int dy = y1-y2;
                    double distance = Math.sqrt(dx*dx + dy * dy);
                    int centerXNew = (x1+x2) / 2;
                    int centerYNew = (y1+y2) / 2;
                    offsetX = centerXNew - centerX;
                    offsetY = centerYNew - centerY;
                    if(distanceOld > 0.1){
                        zoomFactor = zoomFactorOld * distance/ distanceOld;
                    }


                    if(zoomFactor < 0){
                        zoomFactor *= -1;
                    }
                    if(zoomFactor < 0.5){
                        zoomFactor = 0.5;
                    }
                    if(zoomFactor > 3){
                        zoomFactor = 3;
                    }
                    offsetX *= distance/ distanceOld;
                    offsetY *= distance/ distanceOld;
                    invalidateView();
                    break;
                }
            case MotionEvent.ACTION_DOWN:
                if(pointerID == INVALID){
                    pointerID = motionEvent.getPointerId(0);
                    int pointerI = motionEvent.findPointerIndex(pointerID);
                    if(p1 == null){
                        p1 = new MotionEvent.PointerCoords();
                    }
                    motionEvent.getPointerCoords(pointerI, p1);
                }else if(pointerID2 == INVALID ){
                    for(int i = 0; i < motionEvent.getPointerCount(); i++) {
                        if(motionEvent.getPointerId(i) == pointerID){
                            continue;
                        }

                        pointerID2 = motionEvent.getPointerId(i);
                        if(p2 == null){
                            p2 = new MotionEvent.PointerCoords();
                        }
                        motionEvent.getPointerCoords(i, p2);
                        int x1 = (int)p1.x;
                        int y1 = (int)p1.y;
                        int x2 = (int)p2.x;
                        int y2 = (int)p2.y;
                        int dx = x1-x2;
                        int dy = y1-y2;
                        distanceOld = Math.sqrt(dx*dx + dy * dy);
                        centerX = (x1+x2) / 2 - offsetX;
                        centerY = (y1+y2) / 2 - offsetY;
                        break;
                    }

                }

                break;




            case MotionEvent.ACTION_POINTER_UP:
                if(motionEvent.getPointerId(0) == pointerID) {
                    zoomFactorOld = zoomFactor;
                    //don't break;
                }else if(motionEvent.getPointerId(0) == pointerID2){
                    pointerID2 = INVALID;
                    zoomFactorOld = zoomFactor;
                    break;
                }else{
                    break;
                }

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                pointerID = INVALID;
                pointerID2 = INVALID;
                break;
            default:
                break;




            }
        return true;
    }


    private void invalidateView(){
        createTmpBitmap();
        if(v != null){
            v.invalidate();
        }
    }

    public void updateSize(int width, int height) {
        if(bitmap == null){
            this.width = 1;
            this.height = 1;
            return;
        }
        float f = Math.min((float) width / bitmap.getWidth(), (float)height / bitmap.getHeight());
        this.width = (int)(bitmap.getWidth() * f);
        this.height = (int)(bitmap.getHeight() * f);
        if(this.width <= 0){
            this.width = 10;
            //TODO
        }
        if(this.height <= 0){
            this.height = 10;
            //TODO
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
