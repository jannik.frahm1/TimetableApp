/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.ui.activity;

import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import timetable.com.gmail.pietsch.josia.timetable.R;
import timetable.com.gmail.pietsch.josia.timetable.data.TimetableDataManager;
import timetable.com.gmail.pietsch.josia.timetable.ui.DialogHelper;

public class MiscInfoActivity extends AppCompatActivity {
    DialogFragment loading;
    final String tag = "TimetableActivity";
    LinearLayout root;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_misc_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(timetable.com.gmail.pietsch.josia.timetable.R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loading = DialogHelper.createSimpleDialog("Laden...");
        loading.setCancelable(false);
        loading.show(this.getFragmentManager(), "miscMsg1");
        root = (LinearLayout) findViewById(R.id.miscImgsRoot);
        loadData();
    }
    private void loadData() {
        AsyncTask<Void, Void, Integer> t = new AsyncTask<Void, Void, Integer>() {

            @Override
            protected Integer doInBackground(Void[] v) {
                Log.i("TimetableActivity", "Loading data...");
                int i = TimetableDataManager.getInstance().loadMiscImgs();
                return Integer.valueOf(i);
            }

            @Override
            protected void onPostExecute(Integer state) {


                loading.dismiss();
                if (state.intValue() != 0) {
                    String text;
                    switch (state.intValue()) {
                        case 1:
                            text = "Keine Internetverbindung.";
                            break;
                        default:
                            text = "Ein unbekannter Fehler ist aufgetreten.";
                    }
                    DialogFragment errorDialog = DialogHelper.createSimpleDialog(text);
                    errorDialog.show(getFragmentManager(), "ERROR");
                } else {
                    for (Bitmap b : TimetableDataManager.getInstance().getBitmapsMisc()) {
                        ImageView iv = new ImageView(getApplicationContext());
                        iv.setImageBitmap(b);
                        root.addView(iv);


                    }
                }

            }
        };
        t.execute((Void) null);
    }

}
