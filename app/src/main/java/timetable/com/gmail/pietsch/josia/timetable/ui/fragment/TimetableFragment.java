/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.ui.fragment;


import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import timetable.com.gmail.pietsch.josia.timetable.R;
import timetable.com.gmail.pietsch.josia.timetable.data.TimetableDataManager;
import timetable.com.gmail.pietsch.josia.timetable.data.TimetableDataUpdateListener;
import timetable.com.gmail.pietsch.josia.timetable.ui.activity.TimetableActivity;
import timetable.com.gmail.pietsch.josia.timetable.ui.view.BitmapView;

/**
 * Created by jrpie on 14.01.2018.
 */

public class TimetableFragment extends Fragment {
    boolean tomorrow = false;
    boolean secondary = false;
    BitmapView bitmapView = null;
    SwipeRefreshLayout refreshLayout = null;
    private String text = "ERROR";

    private TimetableDataUpdateListener listener = new TimetableDataUpdateListener() {
        @Override
        public void onUpdate() {
            if(refreshLayout != null)
                refreshLayout.setRefreshing(false);
            setImage(tomorrow, secondary);
        }
    };



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.timetable_fragment, container, false);
        if(savedInstanceState != null){
            loadState(savedInstanceState);
        }
        bitmapView = (BitmapView) view.findViewById(R.id.bitmapView);
        bitmapView.setText(text);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Toast.makeText(getContext(), "LADEN...", Toast.LENGTH_SHORT).show();
                        TimetableDataManager.getInstance().reload();

                    }
                }
        );

        TimetableDataManager.getInstance().addListener(listener);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateText();
        if(TimetableDataManager.getInstance().getState() > TimetableDataManager.STATE_BUSY){
            setImage(tomorrow, secondary);
        }
    }

    private void loadState(Bundle b){
        if(b.containsKey("tomorrow")){
            tomorrow = b.getBoolean("tomorrow");
        }
        if(b.containsKey("secondary")){
            secondary = b.getBoolean("secondary");
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState){
        outState.putBoolean("tomorrow", tomorrow);
        outState.putBoolean("secondary", secondary);
        super.onSaveInstanceState(outState);
    }

    public void setImage(boolean tomorrow, boolean secondary){
        this.tomorrow = tomorrow;
        this.secondary = secondary;

        if(bitmapView != null) {
            bitmapView.setBitmap(getImage());
        }
    }
    private Bitmap getImage(){
        if(tomorrow){
            return TimetableDataManager.getInstance().getBitmapForTomorrow(secondary);
        }
        return TimetableDataManager.getInstance().getBitmapForToday(secondary);

    }

    public void updateText() {
        String text = "ERROR";
        switch(TimetableDataManager.getInstance().getState()){
            case TimetableDataManager.STATE_NO_DATA:
            case TimetableDataManager.STATE_BUSY:
                text = "Laden...";
                break;
            case TimetableDataManager.STATE_DATA_LOADED:
                text = "Nicht gefunden.";
                break;

            default:break;
        }
        this.text = text;
        if(bitmapView != null && text  != null){
            bitmapView.setText(text);
        }

    }

    public void setSecondary(boolean secondary) {
        this.secondary = secondary;
        setImage(tomorrow,secondary);
    }

    public void displayTomorrow(boolean tomorrow) {
        this.tomorrow = tomorrow;
        setImage(tomorrow,secondary);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TimetableDataManager.getInstance().removeListener(listener);
    }
}
