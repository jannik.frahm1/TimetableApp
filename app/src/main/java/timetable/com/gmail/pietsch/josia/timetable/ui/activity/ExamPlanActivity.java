package timetable.com.gmail.pietsch.josia.timetable.ui.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

import timetable.com.gmail.pietsch.josia.timetable.R;
import timetable.com.gmail.pietsch.josia.timetable.data.ServerData;

public class ExamPlanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_plan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WebView webView = (WebView) findViewById(R.id.examWebView);


        webView.loadData("<html><body><h1>Test</h1></body></html>", "text/html", "UTF-8");

        webView.loadUrl(ServerData.downloadPageURL);
    }

}
