/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public final class HTTPHelper {
	public static final HTTPHelper instance = new HTTPHelper();

	private HTTPHelper() {

	}


	public static HTTPHelper getInstance() {
		return instance;
	}
	public String requestData() throws IOException {
		URL url = new URL(ServerData.serverURL);
		Map<String, String> parameters = ServerData.getParameters();

		HttpURLConnection http = (HttpURLConnection) url.openConnection();


        http.setRequestMethod("POST");

        http.setDoInput(true);
		http.setDoOutput(true);

		ServerData.setRequestProperties(http);

		DataOutputStream out = new DataOutputStream(http.getOutputStream());
		out.writeBytes(getParamsString(parameters));
		out.flush();
		out.close();

		//int status = http.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();

		return content.toString();
	}
	public String getImageURL(String name){
		return (ServerData.imageURL+name);
	}
	public Bitmap getBitmap(String name) throws IOException{
		Bitmap img = null;
		URL url = new URL(getImageURL(name));
		InputStream in = url.openStream();
		img = BitmapFactory.decodeStream(in);
		return img;
	}
	public MenuPlan getMenuPlan() throws IOException{
		URL url = new URL(ServerData.menuPlanURL);

		HttpURLConnection http = (HttpURLConnection) url.openConnection();

		BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		boolean read = false;
		while ((inputLine = in.readLine()) != null) {
			if(read){
				if(inputLine.startsWith("</table>")){
					read = false;
					break;
				}else{
					content.append(inputLine);
				}

			}else if(inputLine.startsWith("<table")){
				content.append("<table>");
				read = true;
			}

		}
		in.close();
		content.append("</table></div></body></html>");
		String s = content.toString();
		s = s.replaceAll("nowrap=\"nowrap\" style=\"", "style=\"//");
		Log.d("HTML!!", s);
		return MenuPlan.fromHTMLTable(s);
	}
	private static String getParamsString(Map<String, String> params) throws UnsupportedEncodingException {
		String result  = "";

		for (Map.Entry<String, String> entry : params.entrySet()) {

			result += URLEncoder.encode(entry.getKey(), "UTF-8") + "=";
			result += URLEncoder.encode(entry.getValue(), "UTF-8")+ "&";
		}

		return result.length() > 0 ? result.substring(0, result.length() - 1) : result;
	}
}
