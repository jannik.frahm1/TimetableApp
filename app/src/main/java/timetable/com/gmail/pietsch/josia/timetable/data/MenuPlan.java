/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.data;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jrpie on 12.03.2018.
 */

public class MenuPlan {
    public List<MenuPlanEntry> entries = new ArrayList<>();

    public MenuPlan(){

    }
    public void add(MenuPlanEntry menuPlanEntry){
        entries.add(menuPlanEntry);
    }

    //quite ugly
    public static MenuPlan fromHTMLTable(String html){
        MenuPlan mp = new MenuPlan();

        String[] tags = html.split("<");
        int read = 0;
        String date = "";
        String dayOfWeek = "";
        String menu = "";
        String cook = "";
        for(String s : tags){
            if(s.startsWith("/tr")){
                if(!date.startsWith("&nbsp;") && !date.trim().equalsIgnoreCase("")){
                    mp.add(new MenuPlanEntry(date, dayOfWeek, menu, cook));
                }
                read = 0;
                date = "";
                dayOfWeek = "";
                menu = "";
                cook = "";
            }else
            if(s.startsWith("p ") || s.startsWith("p>")){
                String[] d = s.split(">");
                String data = "";
                if(d.length > 1){
                    data = d[1].replaceAll("\t","").trim();
                }
                switch (read){
                    case 0:
                        date = data;
                        break;
                    case 1:
                        dayOfWeek = data;
                        break;
                    case 2:
                        menu = data;
                        break;
                    case 3:
                        cook = data;
                        break;
                    default:
                }
                read ++;

            }else
            if(s.startsWith("strong")){
                String[] d = s.split(">");
                String data = "";
                if(d.length > 1){
                    data = d[1].replaceAll("\t","");
                    menu = data;
                }
            }
        }

        return mp;

    }

    public static class MenuPlanEntry {
        String date;
        String dayOfWeek;
        String menu;


        public String getMenu() {
            return menu;
        }

        public void setMenu(String menu) {
            this.menu = menu;
        }
        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public String getCook() {
            return cook;
        }

        public void setCook(String cook) {
            this.cook = cook;
        }

        String cook;

        private MenuPlanEntry(String date, String dayOfWeek, String menu, String cook){
            this.date = date;
            this.dayOfWeek = dayOfWeek;
            this.menu = menu;
            this.cook = cook;
        }
        public String toString(){
            String s = "Date:"+date+" Day:"+dayOfWeek+" Menu:"+menu+" Cook:"+cook;
            return s;
        }
    }
}
