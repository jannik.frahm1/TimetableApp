/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.data;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class XMLHelper {
	public static final XMLHelper instance = new XMLHelper();
	private XMLHelper() {
		
	}
	public static XMLHelper getInstance() {
		return instance;
	}
	public Map<String, String> getImageNames(String xmlData) throws SAXException, IOException, ParserConfigurationException{
		Map<String, String> images = new HashMap<>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

		InputSource data = new InputSource(new StringReader(xmlData));
		Document doc = dBuilder.parse(data);
		
		NodeList res;
		try{
			res = doc.getDocumentElement().getElementsByTagName("Rec");
		}catch(NullPointerException n) {
			return null;
		}
		
		for(int i = 0; i < res.getLength(); i++) {
			Element n = (Element) res.item(i);
			NodeList col = n.getElementsByTagName("Col");
			String name = ((Element)col.item(3)).getTextContent();
			String number = ((Element)col.item(0)).getTextContent();
			String type = ((Element)col.item(4)).getTextContent();
			images.put(name, number+"."+type);
		}
		
		
		return images;
	}
}