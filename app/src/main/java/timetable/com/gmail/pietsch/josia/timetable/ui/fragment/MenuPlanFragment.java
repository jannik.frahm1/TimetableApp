package timetable.com.gmail.pietsch.josia.timetable.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import timetable.com.gmail.pietsch.josia.timetable.R;
import timetable.com.gmail.pietsch.josia.timetable.data.HTTPHelper;
import timetable.com.gmail.pietsch.josia.timetable.data.MenuPlan;


public class MenuPlanFragment extends Fragment {
    private WebView webView;
    private class LoadTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            MenuPlan data;
            try {
                data = HTTPHelper.getInstance().getMenuPlan();
            } catch (Exception e) {
                data = new MenuPlan();
            }
            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.append("<html><body><div>");
            htmlBuilder.append("<table style=\"width:100%;margin: 0px; paddng: 0px;border-spacing:0\">");
            boolean dark = false;
            for (MenuPlan.MenuPlanEntry entry : data.entries) {
                dark = !dark;
                htmlBuilder.append("<tr" + (dark ? " style=\"background-color:#CCCCCC;\">" : ">"));
                htmlBuilder.append("<td>" +
                        "<div><strong>" + entry.getDayOfWeek() + "</strong></div>" +
                        "<div>" + entry.getDate() + "</div>" +
                        "</td>" +
                        "<td>" +
                        "<div><strong>" + entry.getMenu() + "</strong></div>" +
                        (entry.getCook().equals("&nbsp;") || (entry.getCook().trim().equals("")) ? "" : "(" + entry.getCook() + ")") + "" +
                        "</td></tr>");
                if(entry.getDayOfWeek().equalsIgnoreCase("freitag")){
                    htmlBuilder.append("<tr style=\"background-color:#000000; height: 5px;\"><td></td><td></td></tr>");
                }
            }
            if (data.entries.isEmpty()) {
                htmlBuilder.append("<tr><td>Nicht gefunden.</tr></td>");
            }
            htmlBuilder.append("</table><br></br><br></br><br></br><br></br><br></br></div></body></html>");
            return htmlBuilder.toString();

        }

        @Override
        protected void onPostExecute(String s) {
            if(webView != null)
                webView.loadData(s, "text/html", "utf-8");
        }
    };
    private LoadTask loadTask;

    public MenuPlanFragment() {}
    public static MenuPlanFragment newInstance(String param1, String param2) {
        MenuPlanFragment fragment = new MenuPlanFragment();
        //Bundle args = new Bundle();
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_menu_plan, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webView = (WebView) view.findViewById(R.id.webView);
        loadTask = new LoadTask();
        loadTask.execute((Void[]) null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
