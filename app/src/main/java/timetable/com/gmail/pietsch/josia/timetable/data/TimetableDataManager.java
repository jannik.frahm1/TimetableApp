/*
 * Copyright 2018 jrpie
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.data;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Singleton
 * Created by jrpie on 12.01.2018.
 */
public final class TimetableDataManager {
    public static TimetableDataManager instance;

    private List<TimetableDataUpdateListener> listeners;

    public static final int STATE_NO_DATA = 0;
    public static final int STATE_BUSY = 1;
    public static final int STATE_DATA_LOADED = 2;
    private Map<String, String> imgs = new HashMap<String, String>();
    private List<String> today = new ArrayList<String>();
    private List<String> tomorrow = new ArrayList<>();
    private List<String> misc = new ArrayList<>();
    private Bitmap todayImg1 = null;
    private Bitmap todayImg2 = null;
    private Bitmap tomorrowImg1 = null;
    private Bitmap tomorrowImg2 = null;
    private List<Bitmap> miscImgs = new ArrayList<>();
    private boolean displaySecondary = false;
    private int state = 0;

    public synchronized void load() {
        if (state > 0) {
            return;
        }
        state = STATE_BUSY;
        AsyncTask loadTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {

                try {
                    String s = HTTPHelper.getInstance().requestData();
                    imgs = XMLHelper.getInstance().getImageNames(s);
                    today = TimetableHelper.getInstance().getNamesForToday(imgs.keySet());
                    tomorrow = TimetableHelper.getInstance().getNamesForNextDay(imgs.keySet());
                    misc = TimetableHelper.getInstance().getMiscNames(imgs.keySet());
                    HTTPHelper http = HTTPHelper.getInstance();
                    for (String str : today) {

                        String name = str.split("\\.")[0];
                        if (name.endsWith("1")) {
                            todayImg1 = http.getBitmap(imgs.get(str));
                        } else if (name.endsWith("2")) {
                            todayImg2 = http.getBitmap(imgs.get(str));
                        }
                    }
                    for (String str : tomorrow) {
                        String name = str.split("\\.")[0];
                        if (name.endsWith("1")) {
                            tomorrowImg1 = http.getBitmap(imgs.get(str));
                        } else if (name.endsWith("2")) {
                            tomorrowImg2 = http.getBitmap(imgs.get(str));
                        }
                    }

                } catch (Exception e) {
                    state = STATE_DATA_LOADED;
                    Log.w("TimetableActivity", "Exception loading bitmaps: " + e.getLocalizedMessage());
                    return 1;
                }
                state = STATE_DATA_LOADED;
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                callListeners();
            }
        };
        loadTask.execute();
    }

    public synchronized void reload() {
        state = STATE_NO_DATA;
        load();
    }

    public int loadMiscImgs() {
        try {
            HTTPHelper http = HTTPHelper.getInstance();
            while(misc.size() > 10){
                misc.remove(10);
            }
            if (miscImgs.isEmpty())
                for (String str : misc) {
                    Bitmap tempImg = http.getBitmap(imgs.get(str));
                    miscImgs.add(tempImg);
                }
        } catch (Exception e) {
            return 1;
        }
        //TODO:
        //  callListeners() has to be called from the UI Thread
        // callListeners();
        return 0;

    }

    public Bitmap getBitmapForToday(boolean sec) {
        return sec ? todayImg2 : todayImg1;
    }

    public List<Bitmap> getBitmapsMisc() {
        return miscImgs;
    }

    public Bitmap getBitmapForTomorrow(boolean sec) {
        return sec ? tomorrowImg2 : tomorrowImg1;
    }

    public int getState() {
        return state;
    }


    public synchronized void callListeners() {
        synchronized (listeners) {
            for (TimetableDataUpdateListener listener : listeners) {
                listener.onUpdate();
            }
        }

    }

    public void addListener(TimetableDataUpdateListener listener) {
        synchronized (listeners) {
            listeners.add(listener);
        }

    }

    public synchronized void removeListener(TimetableDataUpdateListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }

    }

    public static TimetableDataManager getInstance() {
        if (instance == null) {
            instance = new TimetableDataManager();
        }
        return instance;
    }


    private TimetableDataManager() {
        listeners = new ArrayList<TimetableDataUpdateListener>();
    }

}
