/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


public class BitmapView extends View {
    private float offsetX = 0;
    private float offsetY = 0;
    private int swipeXStart = 0;
    private int swipeYStart = 0;
    private TextPaint mTextPaint;
    private Paint p = new Paint();
    private Bitmap bitmap = null;
    private Bitmap tmp = null;
    private String text = "ERROR";
    private boolean init = false;
    private ZoomHelper zh;

    public BitmapView(Context context) {
        super(context);
        init(null, 0);
    }

    public BitmapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public BitmapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(zh != null){
            zh.updateSize(w, h);
        }

    }

    private void init(AttributeSet attrs, int defStyle) {
        p.setColor(Color.BLACK);
        p.setStrokeCap(Paint.Cap.ROUND);
        p.setStrokeWidth(0.5f);
        p.setAntiAlias(true);

        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);
        mTextPaint.setTextSize(60);
        init = true;
        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();
        if(zh != null){
            zh.updateSize(getWidth(), getHeight());
        }

        this.setOnTouchListener(zh);

    }

    private void invalidateTextPaintAndMeasurements() {

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // canvas.drawRect(0,0,getWidth(), getHeight(), p);

        if(zh != null){
            if(zh.getWidth() == 1){
                zh.updateSize(getWidth(), getHeight());
            }
            canvas.drawBitmap(zh.getScaledBitmap(), zh.getOffsetX(), zh.getOffsetY(), p);
            //canvas.drawBitmap( tmp,0,0,p );
        }else{
            canvas.drawText(text, (getWidth() - mTextPaint.measureText(text))/2, 200, mTextPaint);
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        if(bitmap == null){
            this.bitmap = null;
            zh = null;
            if(init){

                this.setOnTouchListener(null);
                invalidate();
            }

            return;
        }else {
            this.bitmap = bitmap;
            zh = new ZoomHelper(bitmap, this, getWidth(), getHeight());
            if (init) {
                this.setOnTouchListener(zh);
            }
        }
        invalidate();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        invalidate();
    }
}
