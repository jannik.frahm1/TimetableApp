/*
 * Copyright 2018 Josia Pietsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package timetable.com.gmail.pietsch.josia.timetable.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TimetableHelper {
	private static TimetableHelper instance = new TimetableHelper();

	private TimetableHelper(){

	}
	public static TimetableHelper getInstance(){
		return instance;
	}
	public static final String[] ignore = {
			"Hintergrund-Gruen"
	};
	public List<String> getNamesForNextDay(Iterable<String> strings) {
		List<String> next = new ArrayList<String>();
		Date today = new Date();
		for (String s : strings) {
			Date d = dateFromFilename(s);
			
			if (d != null && d.compareTo(today) > 0) {
				next.add(s);
			}

		}
		return next;

	}
	public List<String> getMiscNames(Iterable<String> strings) {
		List<String> l = new ArrayList<String>();
		elements: for (String s : strings) {
			for(String i : ignore) {
				if(s.equals(i)) {
					continue elements;
				}
			}
			Date d = dateFromFilename(s);
			if (d == null) {
				l.add(s);
			}
		}
		return l;
	}
	public List<String> getNamesForToday(Iterable<String> strings) {
		List<String> l = new ArrayList<String>();
		Date today = new Date();
		for (String s : strings) {
			Date d = dateFromFilename(s);
			if (d != null && sameDay(d, today)) {
				l.add(s);
			}
		}
		return l;
	}

	public Date dateFromFilename(String s) {
		Date d = null;
		int tmp = 0;
		int day = 0;
		int month = 0;
		int year = 0;
		int field = 0;

		chars: for (char c : s.toCharArray()) {
			if (Character.isDigit(c)) {
				tmp *= 10;
				tmp += Integer.parseInt(String.valueOf(c));
			} else if (c == '-') {
				switch (field) {
				case 0:
					day = tmp;
					break;
				case 1:
					month = tmp;
					break;
				case 2:
					year = tmp;
					break chars;
				default:
					break chars;
				}
				tmp = 0;
				field++;

			} else {
				return null;
			}
		}
		d = new GregorianCalendar(year, month-1, day).getTime();
		return d;
	}

	private boolean sameDay(Date d1, Date d2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(d1);
		cal2.setTime(d2);
		return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
	}
}
